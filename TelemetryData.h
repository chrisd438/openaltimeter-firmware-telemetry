/*
    openaltimeter -- an open-source altimeter for RC aircraft
    Copyright (C) 2010  Jony Hudson
    http://openaltimeter.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TELEMETRYDATA_H
#define TELEMETRYDATA_H

#include "WProgram.h"
#include "Datastore.h"

class TelemetryData
{
  public:
    TelemetryData();
    LogEntry* pLogEntry;
    float currentHeight;
    float maxLaunchHeight;
    float maxHeight;
    float launchWindowEndHeight;
    int numberOfCells;
};

#endif /*TELEMETRYDATA_H*/
