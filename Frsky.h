/*
    openaltimeter -- an open-source altimeter for RC aircraft
    Copyright (C) 2010  Jony Hudson
    http://openaltimeter.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FRSKY_H
#define FRSKY_H

#include "WProgram.h"
#include "Telemetry.h"
#include "TelemetryData.h"
#include "NewSoftSerial.h"

// frsky frame timing
#define FRAME1_PERIOD_MS 200
#define FRAME2_PERIOD_MS 1000
#define FRAME3_PERIOD_MS 5000

// set up to use the servo pin
#define PIN_SERIAL_TX 9

#define FRSKY_USERDATA_BARO_ALT_OFFSET   0x00
#define FRSKY_USERDATA_GPS_ALT_B    0x01
#define FRSKY_USERDATA_TEMP1        0x02
#define FRSKY_USERDATA_RPM          0x03
#define FRSKY_USERDATA_FUEL         0x04
#define FRSKY_USERDATA_TEMP2        0x05
#define FRSKY_USERDATA_CELL_VOLT    0x06

#define FRSKY_USERDATA_GPS_ALT_A    0x09
#define FRSKY_USERDATA_BARO_ALT_B   0x10
#define FRSKY_USERDATA_GPS_SPEED_B  0x11
#define FRSKY_USERDATA_GPS_LONG_B   0x12
#define FRSKY_USERDATA_GPS_LAT_B    0x13
#define FRSKY_USERDATA_GPS_CURSE_B  0x14
#define FRSKY_USERDATA_GPS_DM       0x15
#define FRSKY_USERDATA_GPS_YEAR     0x16
#define FRSKY_USERDATA_GPS_HM       0x17
#define FRSKY_USERDATA_GPS_SEC      0x18
#define FRSKY_USERDATA_GPS_SPEED_A  0x19
#define FRSKY_USERDATA_GPS_LONG_A   0x1A
#define FRSKY_USERDATA_GPS_LAT_A    0x1B
#define FRSKY_USERDATA_GPS_CURSE_A  0x1C

#define FRSKY_USERDATA_BARO_ALT_A   0x21
#define FRSKY_USERDATA_GPS_LONG_EW  0x22
#define FRSKY_USERDATA_GPS_LAT_EW   0x23
#define FRSKY_USERDATA_ACC_X        0x24
#define FRSKY_USERDATA_ACC_Y        0x25
#define FRSKY_USERDATA_ACC_Z        0x26

#define FRSKY_USERDATA_CURRENT      0x28

#define FRSKY_USERDATA_VERT_SPEED   0x30 // open9x Vario Mode Only
#define FRSKY_USERDATA_GPS_OFFSET   0x38 // GPS Altitude Offset
#define FRSKY_USERDATA_VFAS_NEW     0x39 // open9x Vario Mode Only

#define FRSKY_USERDATA_VOLTAGE_B    0x3A
#define FRSKY_USERDATA_VOLTAGE_A    0x3B

#define FRSKY_ALT_OFFSET 1

class Frsky : public Telemetry
{
  public:
    Frsky();
    virtual void setup(Settings *settings);
    virtual void processTelemetry(TelemetryData* pTelemetryData, boolean force=false);
    virtual void setEnabled(boolean enabled); 
    virtual void test();
  private:
    uint32_t _frame1MillisCounter, _frame2MillisCounter;
    uint32_t _frame1Interval, _frame2Interval;
    NewSoftSerial* _pSerialOut;
    boolean _enabled;
    void processFrame1(TelemetryData* pTelemetryData);
    void processFrame2(TelemetryData* pTelemetryData);
    void initTelemetry();
    void sendGPSAltitude(float altitude, int16_t offset);
    void sendAltitude(float altitude, int16_t offset);
    void sendTemperature(int32_t temperature);
    void sendVoltage(float voltage, uint16_t numberOfCells);
    void sendCellVoltage(uint8_t cellID, uint16_t voltage);
    void sendValue(uint8_t ID, int16_t value);
    void sendTail();
    
    //static unsigned char frame1[49];
    //static unsigned char frame2[53];
    //static unsigned char frame3[17];
};

#endif /*FRSKY_H*/
