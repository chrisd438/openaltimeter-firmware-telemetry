/*
    openaltimeter -- an open-source altimeter for RC aircraft
    Copyright (C) 2010  Jony Hudson
    http://openaltimeter.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Frsky.h"

/*
// test frame data
unsigned char Frsky::frame1[] = {
        0x5e, 0x24, 0x00, 0x00,   // AccX
        0x5e, 0x25, 0x00, 0x00,   // AccY
        0x5e, 0x26, 0x00, 0xfe,   // AccZ
        0x5e, 0x10, 0x01, 0x00,   // Altitude (before .)
        0x5e, 0x21, 0x01, 0x00,   // Altitude (after .)
        0x5e, 0x02, 0x01, 0x00,   // Temp1
        0x5e, 0x05, 0x01, 0x00,   // Temp2
        0x5e, 0x06, 0x00, 0x00,   // Lipo volt
        0x5e, 0x28, 0x00, 0x00,   // Amp amp
        0x5e, 0x3a, 0x00, 0x00,   // Amp volt (before .)
        0x5e, 0x3b, 0x00, 0x00,   // Amp volt (after .)
        0x5e, 0x03, 0x00, 0x00,   // RPM
        0x5e
};

unsigned char Frsky::frame2[] = {
        0x5e, 0x14, 0x2c, 0x00,   // GPS course (before .)
        0x5e, 0x1c, 0x03, 0x00,   // GPS course (after .)
        0x5e, 0x13, 0x38, 0x0c,   // GPS latitude (before .)
        0x5e, 0x1b, 0xc9, 0x06,   // GPS latitude (after .)
        0x5e, 0x23, 0x4e, 0x00,   // N/S
        0x5e, 0x12, 0xef, 0x2e,   // GPS longitude (before .)
        0x5e, 0x1a, 0x98, 0x26,   // GPS longitude (after .)
        0x5e, 0x22, 0x45, 0x00,   // E/W
        0x5e, 0x11, 0x02, 0x00,   // GPS speed (before .)
        0x5e, 0x19, 0x93, 0x00,   // GPS speed (after .)
        0x5e, 0x01, 0x18, 0x00,   // GPS altitude (before .)
        0x5e, 0x09, 0x05, 0x00,   // GPS altitude (after .)
        0x5e, 0x04, 0x00, 0x00,   // Fuel level
        0x5e
};

unsigned char Frsky::frame3[] = {
        0x5e, 0x15, 0x0f, 0x07,   // Day - month
        0x5e, 0x16, 0x0b, 0x00,   // Year
        0x5e, 0x17, 0x06, 0x12,   // Hours - minutes
        0x5e, 0x18, 0x32, 0x00,   // Seconds
        0x5e
};
*/

Frsky::Frsky()
{
}

void Frsky::setup(Settings *settings)
{
  _pSerialOut = new NewSoftSerial(PIN_SERIAL_TX, true);
  _pSerialOut->begin(9600);
  _frame1MillisCounter = millis();
  _frame2MillisCounter = millis();
  //set the frame 1 interval
  _frame1Interval = 1000; //settings->logIntervalMS;
  //use the default for frame 2
  _frame2Interval = FRAME2_PERIOD_MS;
  initTelemetry();
  _enabled = true;
  Serial.println("Frsky setup complete.");
}

void Frsky::initTelemetry()
{
  // send in initial GPS height
  sendGPSAltitude(0, FRSKY_ALT_OFFSET);
  // send in initial Alt
  sendAltitude(0, FRSKY_ALT_OFFSET);
  // send tail
  sendTail();
}

void Frsky::processTelemetry(TelemetryData* pTelemetryData, boolean force)
{
  // check for enabled - mainly used to stop telemetry during a test
  if (!_enabled)
    return;
  
  uint32_t currentMillis = millis();
  
  // check frame1
  if ((currentMillis > _frame1MillisCounter) || force)
  {
    _frame1MillisCounter += _frame1Interval;
    processFrame1(pTelemetryData);
  }
  
  // check frame2
  if ((currentMillis > _frame2MillisCounter) || force)
  {
    _frame2MillisCounter += _frame2Interval;
    processFrame2(pTelemetryData);
  } 
  
}

void Frsky::processFrame1(TelemetryData* pTelemetryData)
{
  // send temperature
  sendTemperature(pTelemetryData->pLogEntry->getTemperature()/10);
  // send voltage
  sendVoltage(pTelemetryData->pLogEntry->getBattery(), pTelemetryData->numberOfCells);
  // short delay is needed here for the DHT-U
  delay(50);
  // send altitude
  sendAltitude(pTelemetryData->currentHeight, FRSKY_ALT_OFFSET);
  // send tail
  sendTail();
}

void Frsky::processFrame2(TelemetryData* pTelemetryData)
{
  // send max height as GPS altitude - 0 for now
  sendGPSAltitude(pTelemetryData->maxHeight, FRSKY_ALT_OFFSET);
  // send tail
  sendTail();
}

void Frsky::sendGPSAltitude(float altitude, int16_t offset)
{ 
  // need to send a gps fix before openTX displays GPS data   
  sendValue(FRSKY_USERDATA_GPS_LONG_B, 10);
  sendValue(FRSKY_USERDATA_GPS_LONG_A, 10);      
  sendValue(FRSKY_USERDATA_GPS_LAT_B, 10); 
  sendValue(FRSKY_USERDATA_GPS_LAT_A, 10);

  int16_t b = (int16_t)fabs(altitude);
  int16_t a = (int16_t)((fabs(altitude) - b) * 100);
  
  if (altitude < 0)
    b *= -1;

  // add the offset
  sendValue(FRSKY_USERDATA_GPS_ALT_B, b + offset);
  sendValue(FRSKY_USERDATA_GPS_ALT_A, a); 
}

void Frsky::sendAltitude(float altitude, int16_t offset)
{
  int16_t b = (int16_t)fabs(altitude);
  int16_t a = (int16_t)((fabs(altitude) - b) * 100);
  
  if (altitude < 0)
    b *= -1;
  
  // set the offset
  //sendValue(FRSKY_USERDATA_BARO_ALT_OFFSET, offset);
  // add the offset
  sendValue(FRSKY_USERDATA_BARO_ALT_B, b + offset);
  sendValue(FRSKY_USERDATA_BARO_ALT_A, a);
}

void Frsky::sendTemperature(int32_t temperature)
{
  sendValue(FRSKY_USERDATA_TEMP1, (int16_t)temperature);
}

void Frsky::sendVoltage(float voltage, uint16_t numberOfCells)
{
  for (int i=0; i<numberOfCells; i++)
  { 
    sendCellVoltage(i, (voltage/numberOfCells*2100)/4.2);   
  }
} 

void Frsky::sendCellVoltage(uint8_t cellID, uint16_t voltage)
{
  uint8_t v1 = (voltage & 0x0f00)>>8 | (cellID<<4 & 0xf0);
  uint8_t v2 = (voltage & 0x00ff);
  uint16_t value = (v1 & 0x00ff) | (v2<<8);
  sendValue(FRSKY_USERDATA_CELL_VOLT, value);
}


void Frsky::sendValue(uint8_t ID, int16_t value) 
{
  uint8_t tmp1 = value & 0x00ff;
  uint8_t tmp2 = (value & 0xff00)>>8;
  
  // header
  _pSerialOut->print((uint8_t)0x5E);
  
  _pSerialOut->print((uint8_t)ID);
  if(tmp1 == 0x5E) 
  {
    _pSerialOut->print((uint8_t)0x5D);
    _pSerialOut->print((uint8_t)0x3E);
  }
  else if(tmp1 == 0x5D) 
  {
    _pSerialOut->print((uint8_t)0x5D);
    _pSerialOut->print((uint8_t)0x3D);
  }
  else 
  {
    _pSerialOut->print((uint8_t)tmp1);
  }
  if(tmp2 == 0x5E) 
  {
    _pSerialOut->print((uint8_t)0x5D);
    _pSerialOut->print((uint8_t)0x3E);
  }
  else if(tmp2 == 0x5D) 
  {
    _pSerialOut->print((uint8_t)0x5D);
    _pSerialOut->print((uint8_t)0x3D);
  }
  else 
  {
    _pSerialOut->print((uint8_t)tmp2);
  }
}

void Frsky::sendTail()
{
  // send tail
  _pSerialOut->print((uint8_t)0x5E);
}

void Frsky::setEnabled(boolean enabled)
{
  _enabled = enabled;
}

void Frsky::test()
{
  Serial.println("testing telemetry.");
  _enabled = false;
  
  TelemetryData tdata;
  LogEntry logEntry;
  logEntry.setBattery(7.5);
  
  tdata.pLogEntry = &logEntry;
  for (int i=0; i<100; i++)
  {
    int j = i<50 ? i : 100-i; 
    tdata.currentHeight = j;
    tdata.maxLaunchHeight = j;
    tdata.maxHeight = j;
    tdata.launchWindowEndHeight = j;
    tdata.numberOfCells = 2;
    Serial.print("sending ");
    Serial.println(j);
    processFrame1(&tdata);
    delay(500);
    processFrame2(&tdata);
    //delay(500);
  }
  Serial.println("telemetry test done.");
  _enabled = true;
  
  //_pSerialOut->Print::write(frame1,49);
  //_pSerialOut->Print::write(frame2,53);
  //_pSerialOut->Print::write(frame3,17);
}

